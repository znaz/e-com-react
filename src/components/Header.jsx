import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Header = () => {
  const count = useSelector((state) => state.cart.value);
  return (
    <header className="shadow-lg h-16 px-8 flex justify-between items-center">
      <Link to={'/'}>
        <div className="flex">
          <img src="/images/Logo.png" alt="logo" className="w-20" />
          <div className="flex items-center text-xl text-black font-bold">
            <span className=" font-semibold text-lg">Shopify</span>
          </div>
        </div>
      </Link>
      <div className="flex items-center">
        <nav>
          <ul className="flex gap-4 items-center font-semibold text-sm text-black">
            <Link className=" hover:text-red-400" to={"/"}>
              Home
            </Link>
            <Link className=" hover:text-red-400" to={"/categories"}>
              Categories
            </Link>
            <Link className=" hover:text-red-400" to={"/all-products"}>
              All products
            </Link>
            <Link className=" hover:text-red-400" to={"/all-users"}>
              Users
            </Link>
            <Link
              className=" hover:text-red-400 flex items-center"
              to={"/cart"}
            >
              <img src="/icons/cart.svg" alt="cart" className="w-5" />
              <span className="text-xs text-white bg-red-500 rounded-full w-4 text-center font-semibold">
                {count ? count.length : 0}
              </span>
            </Link>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
