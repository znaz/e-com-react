import React from 'react'
import { Link } from 'react-router-dom'

const ProductCard = ({product}) => {
  return (
    <li>
      <Link to={`/product/${product.id}`}>
        <img
          className=" w-full h-72 transition duration-300 ease-in-out  hover:scale-110"
          src={product.images[1]}
          alt={product.title}
        />
        <h3 className="font-semibold text-md mt-2 ">
          {product.title}
        </h3>
        <span className="text-lg text-red-500">${product.price}</span>
      </Link>
    </li>
  )
}

export default ProductCard