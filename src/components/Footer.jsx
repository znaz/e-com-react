import React from "react";

const Footer = () => {
  return (
    <footer className="flex flex-col gap-6 p-4 mt-24 bg-blue-100/80">
      <div>
        <ul className="flex flex-row justify-center gap-4 font-semibold">
          <li>Help</li>
          <li>Policies</li>
          <li>Contact</li>
        </ul>
      </div>
      <div className="flex flex-row justify-center items-center"></div>
      <h3 className="font-bold text-3xl text-center">SHOPIFY</h3>
      <span className="text-center">
        Shopify , 119 Marylebone Road, England. Copyright © UNIQUE (UK) LIMITED.
        All rights reserved
      </span>
    </footer>
  );
};

export default Footer;
