import { createSlice } from '@reduxjs/toolkit'

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    value: JSON.parse(localStorage.getItem('cart')) || []
  },
  reducers: {
   addToCart : (state,action)=>{
    state.value = [...state.value,action.payload]
    localStorage.setItem('cart', JSON.stringify(state.value)); 
   },
   removeFromCart: (state, action) => {
    state.value = state.value.filter(item => item.id !== action.payload.id);
    localStorage.setItem('cart', JSON.stringify(state.value)); 
  }
  }
})

export const { addToCart, removeFromCart } = cartSlice.actions

export default cartSlice.reducer