import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { addToCart } from "../features/cart/cartSlice";

const ProductPage = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const [product, setProduct] = useState(null);
  const [loading, setLoading] = useState(true);

  const productId = params.productId;

  const handlecart = () => {
    dispatch(addToCart(product));
  };
  useEffect(() => {
    axios
      .get(`https://api.escuelajs.co/api/v1/products/${productId}`)
      .then((res) => {
        setProduct(res.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, [productId]);
  if (loading) {
    return (
      <div className="flex h-screen justify-center items-center text-3xl font-semibold">
        Loading...
      </div>
    );
  }

  return (
    <main className="container mx-auto max-w-[1200px]">
      <section className="flex flex-row  mt-20  gap-10">
        <img
          className="w-64 h-72"
          src={product.images[0]}
          alt={product.title}
        />
        <div className="flex flex-col gap-16">
          <h2 className="font-semibold text-3xl font-serif">{product.title}</h2>
          <p className=" mr-48 text-gray-500  font-serif">
            {product.description}
          </p>
          <span className="font-semibold">
            MRP : &#x20B9;{product.price}{" "}
            <span className="text-gray-400 ml-4 text-sm">
              inclusive of all taxes
            </span>
          </span>

          <div className="flex  gap-4">
            <Link
              onClick={handlecart}
              to={"/cart"}
              className="rounded text-center bg-blue-600  text-white w-28 px-2 py-2 text-sm font-semibold"
            >
              Add to cart
            </Link>
          </div>
        </div>
      </section>
    </main>
  );
};

export default ProductPage;
