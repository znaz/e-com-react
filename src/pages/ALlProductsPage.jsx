import React, { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import axios from "axios";

const ALlProductsPage = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  useEffect(() => {
    axios
      .get("https://api.escuelajs.co/api/v1/products")
      .then((res) => {
        setProducts(res.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setError(err);
        setLoading(false);
      });
  }, []);
  if (loading) {
    return (
      <div className="flex h-screen justify-center items-center text-3xl font-semibold">
        Loading...
      </div>
    );
  }

  if (error || !products) {
    return <div>Failed to load product data.</div>;
  }
  return (
    <main className="container max-w-[1200px] mx-auto">
      <section className="mt-20 container mx-auto">
        <h2 className="text-center text-4xl mb-10 font-semibold">
          All Products
        </h2>
        <div>
          <ul className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  gap-24">
            {products.map((product) => {
              return <ProductCard key={product.id} product={product} />;
            })}
          </ul>
        </div>
      </section>
    </main>
  );
};

export default ALlProductsPage;
