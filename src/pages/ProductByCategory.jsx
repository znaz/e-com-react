import React, { useEffect, useState } from 'react'
import ProductCard from '../components/ProductCard';
import { useParams } from 'react-router-dom';
import axios from 'axios';

const ProductByCategory = () => {
    const [products, setProducts] = useState(null);
    const params = useParams();
    const [loading, setLoading] = useState(true);
    const categoryId = params.categoryId;
    useEffect(() => {
      axios
        .get(`https://api.escuelajs.co/api/v1/categories/${categoryId}/products`)
        .then((res) => {
          setProducts(res.data);
          setLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
        });
    }, [categoryId]);
    if (loading) {
      return <div className='flex h-screen justify-center items-center text-3xl font-semibold'>Loading...</div>;
    }
  
    return (
      <main className="container mx-auto max-w-[1200px]">
        <section className="flex flex-col  mt-20  gap-10">
          <h2 className='text-4xl font-semibold'>{products[0].category.name}</h2>
          <div>
            <ul className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  gap-24">
              {products.slice(0,8).map((product) => {
                return (
                  <ProductCard key={product.id} product={product}/>
                );
              })}
            </ul>
          </div>
        </section>
      </main>
  )
}

export default ProductByCategory