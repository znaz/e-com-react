import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { removeFromCart } from '../features/cart/cartSlice'

const CartPage = () => {
    const cartItems = useSelector((state)=> state.cart.value)
    const dispatch = useDispatch()
    if(cartItems.length === 0){
        return (
            <main className='container max-w-[1200px] mx-auto flex justify-center items-center'>
                <div className='text-4xl text-gray-600 '>Your cart is empty</div>
            </main>
        )
    }
  return (
    <main className='container mx-auto flex flex-col  items-center'>
        <h2 className='text-4xl font-semibold mt-10'>My Cart</h2>
        <div className='className="overflow-x-auto w-full max-w-[1100px] bg-white mt-6 shadow-lg rounded-xl '>
        <table className="w-full text-left text-sm ">
              <thead className="uppercase border-b-2 bg-teal-500  dark:border-neutral-600">
                <tr>
                  <th  className="px-6 py-4">
                    Product
                  </th>
                  <th  className="px-6 py-4">
                    price
                  </th>
                  <th  className="px-6 py-4">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {cartItems.map((item) => {
                  return (
                    <tr
                      key={item.id}
                      className="border-t dark:border-neutral-600"
                    >
                      <th scope="row" className="px-6 py-4">
                        <div className="flex gap-4 items-center">
                          <img className="w-12 h-12" src={item.images[1]} alt={item.title} />
                          <span className='text-lg font-medium'>{item.title}</span>
                        </div>
                      </th>
                      <td className="px-6 py-4 text-red-600 font-semibold">${item.price}</td>
                     
        
                      <td className='text-center'>
                        <button onClick={()=>dispatch(removeFromCart({id : item.id}))} className='bg-red-600 px-2 py-1 font-semibold text-white rounded border-none'>
                            Delete
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
        </div>
    </main>
  )
}

export default CartPage