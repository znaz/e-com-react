import axios from "axios";
import React, { useEffect, useState } from "react";

const AllUsersPage = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  useEffect(() => {
    axios
      .get("https://api.escuelajs.co/api/v1/users")
      .then((res) => {
        setUsers(res.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setError(err);
        setLoading(false);
      });
  }, []);
  if (loading) {
    return (
      <div className="flex h-screen justify-center items-center text-3xl font-semibold">
        Loading...
      </div>
    );
  }

  if (error || !users) {
    return <div>Failed to load user data.</div>;
  }
  return (
    <main className="container">
      <section className="mt-20 max-w-[1100px] mx-auto">
    <h2 className="text-center text-4xl mb-10 font-semibold">All Users</h2>
    <div>
      <table className="border w-full">
        <thead>
          <tr className="bg-teal-400 text-lg">
            <th className="border-2 border-black px-4 py-2">Avatar</th>
            <th className="border-2 border-black px-4 py-2">Name</th>
            <th className="border-2 border-black px-4 py-2">Email</th>
            <th className="border-2 border-black px-4 py-2">Role</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => {
            return (
              <tr key={user.id} className="border-b font-medium border-gray-400">
                <td className="border border-gray-400 px-4 py-2">
                  <img  src={user.avatar} alt="avatar" className="w-16 h-16 rounded-full" />
                </td>
                <td className="border border-gray-400 px-4 py-2">{user.name}</td>
                <td className="border border-gray-400 px-4 py-2">{user.email}</td>
                <td className="border border-gray-400 px-4 py-2">{user.role}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  </section>
    </main>
  );
};

export default AllUsersPage;
