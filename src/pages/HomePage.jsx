import axios from 'axios';
import React, { useEffect, useState } from 'react'
import ProductCard from '../components/ProductCard';

const HomePage = () => {
    const [products, setProducts] = useState([]);

  useEffect(() => {
    axios
      .get("https://api.escuelajs.co/api/v1/products")
      .then((res) => {
        setProducts(res.data);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <main>
      <section>
        <img src="/images/coverr.png" alt="ecommerce cover" />
      </section>
      <section className="mt-20 container mx-auto max-w-[1200px]">
        <h2 className="text-center text-4xl mb-6 font-semibold">Latest Products</h2>
        <div>
          <ul className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  gap-24">
            {products.slice(0,8).map((product) => {
              return (
                <ProductCard key={product.id} product={product}/>
              );
            })}
          </ul>
        </div>
      </section>
    </main>
  )
}

export default HomePage