import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

const CategoriesPage = () => {
    const [categories, setCategories] = useState([]);

    useEffect(() => {
      axios
        .get("https://api.escuelajs.co/api/v1/categories")
        .then((res) => {
          setCategories(res.data);
        })
        .catch((err) => console.log(err));
    }, []);
    return (
      <main className="container max-w-[1200px] mx-auto mt-12">
        <h2 className="text-4xl font-semibold mb-10">All Categories</h2>
        <ul className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  gap-24">
          {
            categories.slice(0,5).map((category)=>{
              return <Link to={`/categories/${category.id}`} key={category.id}>
                <img className="w-full  h-80 object-cover transition-all ease-in duration-300 hover:scale-110" src={category.image} alt={category.name}/>
              <h3 className="text-lg text-center mt-3 font-semibold">{category.name}</h3>
              </Link>
            })
          }
        </ul>
      </main>
  )
}

export default CategoriesPage